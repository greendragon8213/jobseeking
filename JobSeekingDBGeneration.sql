USE [master]
GO
/****** Object:  Database [aspnet-ASPMainProject-20160209022940]    Script Date: 26.02.2016 22:43:55 ******/
CREATE DATABASE [aspnet-ASPMainProject-20160209022940]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'aspnet-ASPMainProject-20160209022940.mdf', FILENAME = N'D:\aspnet-ASPMainProject-20160209022940.mdf' , SIZE = 3136KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'aspnet-ASPMainProject-20160209022940_log.ldf', FILENAME = N'D:\aspnet-ASPMainProject-20160209022940_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [aspnet-ASPMainProject-20160209022940].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET ARITHABORT OFF 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET  DISABLE_BROKER 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET  MULTI_USER 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET DB_CHAINING OFF 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'aspnet-ASPMainProject-20160209022940', N'ON'
GO
USE [aspnet-ASPMainProject-20160209022940]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 26.02.2016 22:43:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ads]    Script Date: 26.02.2016 22:43:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ads](
	[Id] [uniqueidentifier] NOT NULL,
	[AuthorId] [uniqueidentifier] NOT NULL,
	[Content] [varchar](max) NULL,
	[CreationDate] [datetime] NOT NULL,
	[Company] [varchar](30) NULL,
	[ActualState] [int] NOT NULL,
	[Location] [varchar](30) NULL,
	[Position] [varchar](50) NULL,
	[SalaryPerMonth] [int] NULL,
 CONSTRAINT [PK_dbo.Ads] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ads2KeyWords]    Script Date: 26.02.2016 22:43:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ads2KeyWords](
	[Id] [uniqueidentifier] NOT NULL,
	[AdId] [uniqueidentifier] NOT NULL,
	[KeyWordId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_dbo.Ads2KeyWords] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Candidates]    Script Date: 26.02.2016 22:43:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Candidates](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[ExpectedPosition] [varchar](50) NULL,
	[Location] [varchar](30) NULL,
	[SalaryPerMonth] [int] NULL,
	[ExperienceYears] [int] NULL,
	[ExperienceDescription] [varchar](max) NULL,
	[Skills] [varchar](max) NULL,
 CONSTRAINT [PK_dbo.Candidates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Candidates2KeyWords]    Script Date: 26.02.2016 22:43:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Candidates2KeyWords](
	[Id] [uniqueidentifier] NOT NULL,
	[CandidateId] [uniqueidentifier] NOT NULL,
	[KeyWordsId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_dbo.Candidates2KeyWords] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Chats]    Script Date: 26.02.2016 22:43:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Chats](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId1] [uniqueidentifier] NOT NULL,
	[UserId2] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_dbo.Chats] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employers]    Script Date: 26.02.2016 22:43:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employers](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[FullName] [varchar](15) NULL,
	[ContactInformation] [varchar](30) NULL,
	[Company] [varchar](30) NULL,
 CONSTRAINT [PK_dbo.Employers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KeyWords]    Script Date: 26.02.2016 22:43:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KeyWords](
	[Id] [uniqueidentifier] NOT NULL,
	[Value] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Messages]    Script Date: 26.02.2016 22:43:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Messages](
	[Id] [uniqueidentifier] NOT NULL,
	[AuthorId] [uniqueidentifier] NOT NULL,
	[ChatId] [uniqueidentifier] NOT NULL,
	[Text] [varchar](max) NOT NULL,
	[Date] [datetime] NOT NULL,
	[IsRead] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Messages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 26.02.2016 22:43:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [uniqueidentifier] NOT NULL,
	[UserName] [varchar](64) NOT NULL,
	[Password] [varchar](64) NOT NULL,
	[Role] [int] NOT NULL,
	[Position] [int] NOT NULL,
	[ApprovingStatus] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_AuthorId]    Script Date: 26.02.2016 22:43:55 ******/
CREATE NONCLUSTERED INDEX [IX_AuthorId] ON [dbo].[Ads]
(
	[AuthorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AdId]    Script Date: 26.02.2016 22:43:55 ******/
CREATE NONCLUSTERED INDEX [IX_AdId] ON [dbo].[Ads2KeyWords]
(
	[AdId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_KeyWordId]    Script Date: 26.02.2016 22:43:55 ******/
CREATE NONCLUSTERED INDEX [IX_KeyWordId] ON [dbo].[Ads2KeyWords]
(
	[KeyWordId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UserId]    Script Date: 26.02.2016 22:43:55 ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[Candidates]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CandidateId]    Script Date: 26.02.2016 22:43:55 ******/
CREATE NONCLUSTERED INDEX [IX_CandidateId] ON [dbo].[Candidates2KeyWords]
(
	[CandidateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_KeyWordsId]    Script Date: 26.02.2016 22:43:55 ******/
CREATE NONCLUSTERED INDEX [IX_KeyWordsId] ON [dbo].[Candidates2KeyWords]
(
	[KeyWordsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UserId1]    Script Date: 26.02.2016 22:43:55 ******/
CREATE NONCLUSTERED INDEX [IX_UserId1] ON [dbo].[Chats]
(
	[UserId1] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UserId2]    Script Date: 26.02.2016 22:43:55 ******/
CREATE NONCLUSTERED INDEX [IX_UserId2] ON [dbo].[Chats]
(
	[UserId2] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UserId]    Script Date: 26.02.2016 22:43:55 ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[Employers]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AuthorId]    Script Date: 26.02.2016 22:43:55 ******/
CREATE NONCLUSTERED INDEX [IX_AuthorId] ON [dbo].[Messages]
(
	[AuthorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ChatId]    Script Date: 26.02.2016 22:43:55 ******/
CREATE NONCLUSTERED INDEX [IX_ChatId] ON [dbo].[Messages]
(
	[ChatId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Ads] ADD  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Ads2KeyWords] ADD  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Candidates] ADD  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Candidates2KeyWords] ADD  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Chats] ADD  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Employers] ADD  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[KeyWords] ADD  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Messages] ADD  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Users] ADD  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Ads]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Ads_dbo.Employers_AuthorId] FOREIGN KEY([AuthorId])
REFERENCES [dbo].[Employers] ([Id])
GO
ALTER TABLE [dbo].[Ads] CHECK CONSTRAINT [FK_dbo.Ads_dbo.Employers_AuthorId]
GO
ALTER TABLE [dbo].[Ads2KeyWords]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Ads2KeyWords_dbo.Ads_AdId] FOREIGN KEY([AdId])
REFERENCES [dbo].[Ads] ([Id])
GO
ALTER TABLE [dbo].[Ads2KeyWords] CHECK CONSTRAINT [FK_dbo.Ads2KeyWords_dbo.Ads_AdId]
GO
ALTER TABLE [dbo].[Ads2KeyWords]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Ads2KeyWords_dbo.KeyWords_KeyWordId] FOREIGN KEY([KeyWordId])
REFERENCES [dbo].[KeyWords] ([Id])
GO
ALTER TABLE [dbo].[Ads2KeyWords] CHECK CONSTRAINT [FK_dbo.Ads2KeyWords_dbo.KeyWords_KeyWordId]
GO
ALTER TABLE [dbo].[Candidates]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Candidates_dbo.Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Candidates] CHECK CONSTRAINT [FK_dbo.Candidates_dbo.Users_UserId]
GO
ALTER TABLE [dbo].[Candidates2KeyWords]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Candidates2KeyWords_dbo.Candidates_CandidateId] FOREIGN KEY([CandidateId])
REFERENCES [dbo].[Candidates] ([Id])
GO
ALTER TABLE [dbo].[Candidates2KeyWords] CHECK CONSTRAINT [FK_dbo.Candidates2KeyWords_dbo.Candidates_CandidateId]
GO
ALTER TABLE [dbo].[Candidates2KeyWords]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Candidates2KeyWords_dbo.KeyWords_KeyWordsId] FOREIGN KEY([KeyWordsId])
REFERENCES [dbo].[KeyWords] ([Id])
GO
ALTER TABLE [dbo].[Candidates2KeyWords] CHECK CONSTRAINT [FK_dbo.Candidates2KeyWords_dbo.KeyWords_KeyWordsId]
GO
ALTER TABLE [dbo].[Chats]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Chats_dbo.Users_UserId1] FOREIGN KEY([UserId1])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Chats] CHECK CONSTRAINT [FK_dbo.Chats_dbo.Users_UserId1]
GO
ALTER TABLE [dbo].[Chats]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Chats_dbo.Users_UserId2] FOREIGN KEY([UserId2])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Chats] CHECK CONSTRAINT [FK_dbo.Chats_dbo.Users_UserId2]
GO
ALTER TABLE [dbo].[Employers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Employers_dbo.Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Employers] CHECK CONSTRAINT [FK_dbo.Employers_dbo.Users_UserId]
GO
ALTER TABLE [dbo].[Messages]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Messages_dbo.Chats_ChatId] FOREIGN KEY([ChatId])
REFERENCES [dbo].[Chats] ([Id])
GO
ALTER TABLE [dbo].[Messages] CHECK CONSTRAINT [FK_dbo.Messages_dbo.Chats_ChatId]
GO
ALTER TABLE [dbo].[Messages]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Messages_dbo.Users_AuthorId] FOREIGN KEY([AuthorId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Messages] CHECK CONSTRAINT [FK_dbo.Messages_dbo.Users_AuthorId]
GO
USE [master]
GO
ALTER DATABASE [aspnet-ASPMainProject-20160209022940] SET  READ_WRITE 
GO
