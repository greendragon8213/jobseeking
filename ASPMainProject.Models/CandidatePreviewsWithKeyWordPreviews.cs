﻿using System.Collections.Generic;

namespace ASPMainProject.Models
{
    public class CandidatePreviewsWithKeyWordPreviews
    {
        public List<CandidatePreview> CandidatePreviews
        {
            get;
            set;
        }

        public List<KeyWordPreview> KeyWordPreviews
        {
            get;
            set;
        }
    }
}
